jupyter nbconvert --to html --execute --ExecutePreprocessor.timeout=3600 zzcorbel.ipynb
jupyter nbconvert --to html --execute --ExecutePreprocessor.timeout=3600 zzcorggd.ipynb
jupyter nbconvert --to html --execute --ExecutePreprocessor.timeout=3600 zzcorlux.ipynb

jupyter nbconvert --to html --execute --ExecutePreprocessor.timeout=3600 zzcorblxprd.ipynb

rm zzcorbel.html
rm zzcorbel.csv

rm zzcorggd.html
rm zzcorggd.csv

rm zzcorlux.html
rm zzcorlux.csv

rm zzcorblxprd.html